# maestro

maestro = Mendeley Annotation Exporter for Saving Text to Review Offline

A simple tool to save your Mendeley highlights and notes to local flat files so 
you can use them elsewhere.

## Status

**v0.1**

This is an experimental work in progress. Please see the [milestones][mm] and 
issues for a better idea of what work is left.

[mm]:https://gitlab.com/royragsdale/maestro/milestones

## Quick Start
1. Register API client at: <http://dev.mendeley.com/myapps.html>
2. Update your configuration: `config.yml`
3. Profit

## Motivation

[Mendeley][m] is great for organizing and reading papers but at the moment your 
highlights and notes (annotations) are locked into their platform and not 
generally very useful outside their application.  This is a shame because they 
have a pretty great pdf annotator.  This is also inspired by [Paperpile's][p] 
beautiful [summaries][s].

[m]:https://www.mendeley.com
[p]:https://paperpile.com/
[s]:https://paperpile.com/features/pdf-annotator

## Overview

At the moment the Mendeley [Annotations API][a] only provides a `BoundingBox` 
for your highlights. This means we have to extract the "highlighted" text from 
the pdf ourselves. We rely on [pdfquery][pq] to do this extraction.

[a]:https://api.mendeley.com/apidocs/docs#!/annotations/getAnnotations
[pq]:https://github.com/jcushman/pdfquery/
