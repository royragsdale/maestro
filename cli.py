import argparse
import yaml
import os.path

def load_config(config_file=os.path.join(
        os.path.expanduser('~'),
        '.maestro/config.yml')):
    ''' Loads config file into a global variable'''
    with open(os.path.abspath(config_file)) as f:
        try:
            config = yaml.load(f)
            if not validate_config(config):
                print "Failed to load in a valid configuration"
                print "Must specify at least client_id and client_secret"
                exit()
            else:
                return config
        except yaml.YAMLError as e:
            print "Error: could not load config file {}".format(config_file)
            print e
            exit()

def ensure_exists(option,config,is_dir=True):
    name = config[option]
    if name[0] ==  '~':
        name = os.path.join(os.path.expanduser('~'),name[2:])
        config[option]=name
    if is_dir and not os.path.exists(name):
        os.makedirs(name)
    elif not os.path.exists(os.path.dirname(name)):
        os.makedirs(os.path.dirname(name))
    return config


def validate_config(config):
    if 'port' not in config:
        config['port'] = 5000

    if 'base_dir' not in config:
        config['base_dir'] = os.path.join(os.path.expanduser('~'),'.maestro')
    config = ensure_exists('base_dir',config)

    if 'pdf_dir' not in config:
        config['pdf_dir'] = os.path.join(config['base_dir'],'pdfs')
    config = ensure_exists('pdf_dir',config)

    if 'cache_dir' not in config:
        config['cache_dir'] = os.path.join(config['base_dir'],'pdf_cache')
    config = ensure_exists('cache_dir',config)

    if 'md_dir' not in config:
        config['cache_dir'] = os.path.join(config['base_dir'],'md')
    config = ensure_exists('md_dir',config)

    if 'db_path' not in config:
        config['db_path'] = os.path.join(config['base_dir'],'maestro_db.sqlite')
    config = ensure_exists('db_path',config, False)

    return (('client_id' in config) and ('client_secret' in config))

def parse_args():
    # Handle command line
    parser = argparse.ArgumentParser(
            description='A tool to save your Mendeley highlights and notes \
                    locally.')
    parser.add_argument("-v", "--verbose",
            action="store_true",
            help="print progress to stdout")
    parser.add_argument("-c", "--config", help="specify path to config file")
    subparsers = parser.add_subparsers(dest='command',title="commands")

    #  subparser for status
    parser_status = subparsers.add_parser('status',help="display metadata")

    # subparser for update
    parser_update = subparsers.add_parser('update',
            help="fetch notes from Mendeley")
    parser_update.add_argument("-f", "--forever",
            action="store_true",
            help="fetch ALL data from Mendeley (not just since last checked")

    # subparser for extract
    parser_extract = subparsers.add_parser('extract',
            help="extract highlights and notes from mendeley documents")
    parser_extract.add_argument("-d", "--document",
            metavar='DOC_ID',
            help="extract a specific document")

    # subparser for generate
    parser_generate = subparsers.add_parser('generate',
            help="generate markdown files from extracted highlights")
    parser_generate.add_argument("-d", "--document",
            metavar='DOC_ID',
            help="generate a specific document")

    args = parser.parse_args()
    return args

