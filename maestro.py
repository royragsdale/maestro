import json
import time
import os

# local imports
import mendeley_utils
import pdf_utils
import storage
import output
import cli

# global to store configuration
config=None

def fetch_documents(since=None):
    ''' Fetch documents from the Mendeley API'''
    storage.update_stats('documents',time.time())
    if since:
        since = time.strftime("%Y-%m-%dT%H:%M:%S",time.gmtime(since[0]))
    for d in mendeley_utils.get_documents(since):
        storage.store_document(d)

def fetch_annotations(since=None):
    ''' Fetch annotations from the Mendeley API'''
    storage.update_stats('annotations',time.time())
    if since:
        since = time.strftime("%Y-%m-%dT%H:%M:%S",time.gmtime(since[0]))
    for a in mendeley_utils.get_annotations(since):
        storage.store_annotation(a)

def extract_annotations(document_id=None):
    ''' Load annotations from db that have not yet been extracted and then
        download the appropriate pdf and perform the extraction.'''

    needed = storage.get_needs_extraction(document_id)
    for n in needed:
        a_id, a_type, a_positions, d_id, file_ids = n
        f = mendeley_utils.get_file_from_doc(d_id)
        pdf_path = os.path.join(config['pdf_dir'],f.file_name)
        print "Extracting from {}".format(f.file_name)
        if not os.path.exists(pdf_path):
            print "Downloading {} to: {}".format(f.file_name, pdf_path)
            f.download(os.path.dirname(pdf_path))
        text = pdf_utils.extract(a_positions, pdf_path, config['cache_dir'])
        storage.store_extracts(a_id, text)

def build_summary(document_id=None):
    annotations =  storage.get_summary(document_id)
    documents = {}
    for row in annotations:
        d_id,d_title,d_auth,d_abs,a_id,a_style,a_text,a_pos,a_mod,e_text = row
        # TODO - this is a top level 1 per document field
        if a_style == "note":
            continue
        pos = json.loads(a_pos)
        page =pos[0]['page']
        text = a_text if a_text else e_text
        y = max([ p['y1'] for p in pos])
        x = min([ p['x0'] for p in pos])
        slug = {'style':a_style,'text': text, 'y': y, 'x':x}
        if d_id in documents:
            documents[d_id]['date'] = max(documents[d_id]['date'],a_mod)
            d_pages = documents[d_id]['pages']
            if page in d_pages:
                d_pages[page].append(slug)
            else:
                d_pages[page] = [slug]
        else:
            documents[d_id] = {
                    'title':d_title,
                    'authors':d_auth,
                    'abstract':d_abs,
                    'date':a_mod,
                    'pages':{page: [slug]}
                    }
    for doc_id, doc in documents.iteritems():
        pages = doc['pages']
        # sort within page
        for num in pages:
            # TODO - 300 is a hack/heurisitc from looking at a few, should find
            #real midpoint
            left_col = [ p for p in pages[num] if p['x'] <= 300]
            right_col = [ p for p in pages[num] if p['x'] >= 300]
            pages[num] = sorted(left_col, key=lambda k: k['y'],reverse=True)
            pages[num] += sorted(right_col, key=lambda k: k['y'],reverse=True)

        # sort pages
        doc['pages'] = sorted(pages.iteritems())

        # Format date
        doc['date'] = time.strftime("%Y-%m-%dT%H:%M:%S",time.gmtime(doc['date']))

    return documents

def generate_output(document_id):
    print "Generating markdown output to: {}".format(config['md_dir'])
    summary = build_summary(document_id)
    for doc_id, doc_data in summary.iteritems():
        print doc_id,doc_data['title']
        markdown = output.generate_markdown(doc_data)
        dest = os.path.join(config['md_dir'],doc_id+'.md')
        output.write_markdown(markdown, dest)

def print_status():
    headers=["Data", "Items","Last Updated"]
    print "{:^11} | {:^8} | {:^19}".format(*headers)
    print "-"*(45)
    for table in ['documents','annotations','extracts']:
        items = storage.count_rows(table)
        updated = storage.get_stats(table)
        if not updated:
            updated = "Not yet processed"
        else:
            updated = time.strftime('%Y-%m-%d %H:%M:%S',
                    time.localtime(updated[0]))
        print "{:<11} | {:^8} | {:^19}".format(table, items, updated)

    # TODO - print directory locations
    # TODO - print storage size



def main():
    global config

    # Handle command line options
    args = cli.parse_args()

    # Configure application
    if args.config:
        config = cli.load_config(args.config)
    else:
        config = cli.load_config()

    # Setup database
    storage.initialize(config['db_path'])

    # Setup templating
    output.initialize('templates')

    # These commands require a Mendeley session
    if args.command == "update" or args.command == "extract":
        if not mendeley_utils.intitialize(config):
            print "Failed to initialize Mendeley session"
            exit()

    # Take action
    if args.command == "status":
        print_status()

    elif args.command == "update":
        d_since=None if args.forever else storage.get_stats('documents')
        a_since=None if args.forever else storage.get_stats('annotations')

        print "Starting document fetch"
        fetch_documents(d_since)
        print "Starting annotation fetch"
        fetch_annotations(a_since)

    elif args.command == "extract":
        print "Starting extraction"
        print args.document
        extract_annotations(args.document)

    elif args.command == "generate":
        generate_output(args.document)
        
if __name__ == "__main__":
    main()
