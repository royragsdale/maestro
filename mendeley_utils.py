import yaml
import json
import os
import oauthlib
import webbrowser
import SocketServer
from  BaseHTTPServer import BaseHTTPRequestHandler

from requests_oauthlib import OAuth2Session

from mendeley import Mendeley
from mendeley.session import MendeleySession
from mendeley.auth import MendeleyAuthorizationCodeTokenRefresher
from mendeley.auth import handle_text_response

# globals to store Mendeley Session and pass request_url
session=None
req_url=None
config=None

class SimpleHandler(BaseHTTPRequestHandler):
    ''' Simple Static Handler to catch the OAUTH response'''
    # disable priting a log line
    def log_message(self, format, *args):
        return
    def do_GET(self):
        global req_url 
        req_url = self.path
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write("<html><head><title>Congrats</title></head>\n  \
                <body><p>Should be authenticated. Please return to your \
                terminal.</p>\n</body></html>")

def save_token(token):
    ''' Save token to flat file for actual application usage'''
    token_path = os.path.join(config['base_dir'],'token.json')
    with open(token_path, 'w') as f:
        os.chmod(token_path, 0o600)
        json.dump(token,f)
        f.close()

def load_token():
    ''' Load in stored token from flat file'''
    try:
        with open(os.path.join(config['base_dir'],'token.json')) as token_json:
            return json.load(token_json)
    except:
        return None

def new_client():
    ''' Returns an client and initialized authenticator'''
    redirect_uri = 'http://localhost:{}/'.format(config['port'])
    mendeley = Mendeley(config['client_id'],config['client_secret'],redirect_uri)
    auth = mendeley.start_authorization_code_flow()
    return mendeley, auth

def is_authenticated():
    ''' Checks if the session can access api data that requires
        authentication'''
    try:
        name = session.profiles.me.display_name
        return True
    except oauthlib.oauth2.rfc6749.errors.TokenExpiredError:
        return False

def oauth_authenticate(config,mendeley,auth):
    ''' Perform oauth authentication.
        Return an authenticated mendeley session and save tokens to a flat file
    '''
    print "Trying full oauth workflow"
    try:
        httpd = SocketServer.TCPServer(("",int(config['port'])),SimpleHandler)
    except:
        print "Error starting server. Likely port {} still bound"
        return None

    # User interaction in browser to complete OAUTH flow
    webbrowser.open(auth.get_login_url(), new=0 , autoraise=True)

    # Start server to catch OAUTH redirect
    httpd.handle_request()
    httpd.server_close()

    # Complete authentication with oauth details
    session = auth.authenticate("http://localhost:{}{}".format(
        config['port'],
        req_url))
    save_token(session.token)

    return session

def session_from_token(token, mendeley):
    ''' Returns a session with a previously obtained token'''
    session =  MendeleySession(mendeley,token=token)
    return session

def refresh_token(token,mendeley,auth):
    '''Given a previously obtained token, refreshes it and returns an
        authenticated session'''
    refresher =  MendeleyAuthorizationCodeTokenRefresher(auth)

    # Manually refresh token due to
    # https://github.com/Mendeley/mendeley-python-sdk/issues/10
    # follows fix from @DavidWiesner otherwise should just be
    # refresher.refresh(mendely_session)
    oauth = OAuth2Session(
            client=refresher.client,
            redirect_uri=refresher.redirect_uri,
            scope=['all'],
            token=token)
    oauth.compliance_hook['access_token_response'] = [handle_text_response]
    try:
        token = oauth.refresh_token(refresher.token_url, auth=auth.auth)
    except:
        print "Refresh failed"
        return None

    save_token(token)

    return MendeleySession(mendeley=mendeley,token=token, refresher=refresher)

def intitialize(configuration):
    ''' Primary utility function to get an authenticated session.
        1. Tries loading and existing token
        2. Tries refreshing an existing token
        3. Falls back to full oauth with user iteration'''
    global session
    global config

    config = configuration
    mendeley, auth =  new_client()
    token = load_token()

    if token:
        session = session_from_token(token,mendeley)
        if not is_authenticated():
            print "Needed to refresh"
            session = refresh_token(token,mendeley,auth)
    if not (session and is_authenticated()):
        # Need user interaction to auth
        session = oauth_authenticate(config, mendeley, auth)

    return session and is_authenticated()

def get_documents(since=None):
    return session.documents.iter(modified_since=since)

def get_annotations(since=None):
    return session.annotations.iter(modified_since=since)

def get_file_from_doc(doc_id):
    return session.documents.get(doc_id).files.list().items[0]
