from jinja2 import Environment, FileSystemLoader

env=None

def initialize(template_dir):
    global env
    env = Environment(loader=FileSystemLoader(template_dir))

def generate_markdown(paper, template_name="paper.md"):
    template = env.get_template(template_name)
    rendered_output = template.render(data=paper)
    return rendered_output

def write_markdown(markdown, file_name):
    with open(file_name, "wb") as f:
        f.write(markdown.encode('utf-8'))
