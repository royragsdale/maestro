from itertools import groupby
import json
from operator import itemgetter
import pdfquery
from pdfquery.cache import FileCache
import sys
import os.path

def get_pdfq(fname, cache_dir="/tmp/"):
    ''' Given the filename of a pdf returns the necessary PDFQuery objects
        by default uses a FileCache to speed up repeated accesses'''
    head, tail = os.path.split(fname)
    pdf_merged = pdfquery.PDFQuery(
            fname,
            parse_tree_cacher = FileCache(os.path.join(cache_dir, tail)))
    pdf_nomerge = pdfquery.PDFQuery(
            fname,
            merge_tags=None,
            parse_tree_cacher = FileCache(os.path.join(cache_dir, tail+"_nm")))

    return (pdf_merged,pdf_nomerge)

def pos_to_bbox(pos,buf=4):
    ''' Given a mendely annotation position convert it into a pdfquery style
        bounding box string and also add a buffer (4 was manually chosen based
        on a small sample of tests). Also preserve the page and vertical
        position.
    '''
    pos_str = "{},{},{},{}".format(
            pos['x0'] - buf,
            pos['y0'] - buf,
            pos['x1'] + buf,
            pos['y1'] + buf)

    return pos_str

def pos_pages(positions):

    all_pages = set()
    [ all_pages.add(p['page']-1) for p in positions ]
    return list(all_pages)

def extract_positions(positions, pdfq):
    ''' Given a list of Mendeley positions extract the corresponding
        highlighted text from the pdf
    '''
    extracted = []
    pdf_merged, pdf_nomerge = pdfq 

    # for efficiency load only the pages that the might need
    all_pages = pos_pages(positions)
    pdf_merged.load(*all_pages)
    pdf_nomerge.load(*all_pages)

    # A highlight consists of one or more bounding boxes we need to extract
    lines = []
    for pos in positions:
        # build bounding box and query for highlight coordinates
        # PDFQuery uses 0 indexed pages
        q = 'LTPage[page_index=\'{}\'] :in_bbox("{}")'.format(
                pos['page'] -1,
                pos_to_bbox(pos))

        match = pdf_merged.pq(q)

        # bounding box matched something in the merged pdf it should be a
        # LTTextLineHorizontal that contains the full text of the line
        if len(match) >= 1:
            lines.append({"text": match.text(),"y0": pos['y0']})

        # bounding box did not match a full line. Need to select the non
        # merged characters and rebuild the string
        else:
            # query non merged pdf to get individual characters
            match = pdf_nomerge.pq(q)
            text = ""
            # got at least one charachter
            if len(match) >= 1:
                prev=match[0].get('x0')
                for x in match:
                    # add a space if this character does not line up with
                    # the previous one. Use a buffer of 1 (~1/4 width) to
                    # catch minor alignment shifts that aren't spaces
                    if float(x.get('x0')) >  float(prev) + 1:
                        text += " "
                    if x.text:
                        text += x.text
                    prev = x.get('x1')
                lines.append({"text": text,"y0": pos['y0']})
            else:
                sys.stderr.write("Error: could not match anything\n")

    # combine extracted bounding boxes based on vertical position
    lines = sorted(lines, key=lambda k: k['y0'],reverse=True)
    text = " ".join([x['text'] for x in lines])
    return text

def extract(positions, fname, cache_dir):
    pdfq = get_pdfq(fname, cache_dir)
    text = extract_positions(json.loads(positions), pdfq)
    return text
