import sqlite3
import json

# global connetion
conn = None

def initialize(db_path):
    ''' Initialize the database '''
    global conn
    conn = sqlite3.connect(db_path)

    annotations = 'CREATE TABLE IF NOT EXISTS annotations (\
        id PRIMARY KEY,\
        type,\
        text,\
        privacy_level,\
        color,\
        created,\
        positions,\
        profile_id,\
        last_modified,\
        document_id)'
    execute(annotations)

    documents = 'CREATE TABLE IF NOT EXISTS documents (\
        iid PRIMARY KEY,\
        title,\
        type,\
        source,\
        year,\
        identifiers,\
        keywords,\
        abstract,\
        authors,\
        created,\
        file_ids,\
        group_id,\
        last_modified,\
        profile_id)'
    execute(documents)

    stats = 'CREATE TABLE IF NOT EXISTS stats (\
        data_name PRIMARY KEY,\
        last_fetched)'
    execute(stats)
 
    extracts = 'CREATE TABLE IF NOT EXISTS extracts (\
        anno_id PRIMARY KEY,\
        text)'
    execute(extracts)

def close():
    ''' Commit changes and close connection to the database '''
    conn.commit()
    conn.close()

def execute(q, args=None):
    ''' Run a query and return the cursor so caller can fetch records if
        applicable '''
    cur = conn.cursor()
    if args:
        cur.execute(q,args)
    else:
        cur.execute(q)
    conn.commit()
    return cur

def authors_to_string(authors):
    ''' Given a Mendeley Authors Object convert it into a string '''
    s = "" 
    if authors:
        for a in authors:
            if a.last_name:
                s+= a.last_name
            if a.first_name:
                s+= ", "+a.first_name
            s+="; "
    return s[:-2]

def store_document(d):
    ''' Insert a Mendeley document into the database. Since the API is ground
        truth, always replace the local version'''
    data = (d.id,
            d.title,
            d.type,
            d.source,
            d.year,
            str(d.identifiers),
            str(d.keywords),
            d.abstract,
            authors_to_string(d.authors),
            d.created.timestamp,
            str([ f.id for f in d.files.iter()]),
            d.group.id if d.group else None,
            d.last_modified.timestamp,
            d.profile.id
            )
    doc_sql = 'INSERT OR REPLACE INTO documents VALUES \
            (?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
    execute(doc_sql, data)

def color_to_list(color):
    if color:
        return [color.r, color.g, color.b]
    else:
        return None

def parse_positions(positions):
    parsed=[]
    if not positions:
        return None
    for p in positions:
        d = {'page': p.page,
            'x0' : p.top_left.x,
            'y0' : p.top_left.y,
            'x1' : p.bottom_right.x,
            'y1' : p.bottom_right.y}
        parsed.append(d)
    return parsed

def store_annotation(a):
    ''' Insert a Mendeley annotation into the database. Since the API is ground
        truth, always replace the local version'''
    data = (a.id,
            a.type,
            a.text,
            a.privacy_level,
            json.dumps(color_to_list(a.color)),
            a.created.timestamp if a.created else None,
            json.dumps(parse_positions(a.positions)),
            a.profile.id,
            a.last_modified.timestamp,
            a.document().id)
    anno_sql = 'INSERT OR REPLACE INTO annotations VALUES \
            (?,?,?,?,?,?,?,?,?,?)'
    execute(anno_sql, data)

def update_stats(data_name, fetched_at):
    ''' Update the last time a table was fetched from the Mendeley API '''

    stats_sql = 'INSERT OR REPLACE INTO stats VALUES (?,?)'
    execute(stats_sql,(data_name, fetched_at))

def get_stats(data_name):
    get_stats_sql ='SELECT last_fetched FROM stats where data_name == ?'
    cur = execute(get_stats_sql,(data_name,))
    return cur.fetchone()

def get_needs_extraction(document_id=None):

    needs_sql = 'SELECT a.id, a.type, a.positions, d.iid, d.file_ids FROM \
                annotations AS a \
                LEFT OUTER JOIN \
                extracts as e \
                ON  e.anno_id == a.id \
                JOIN \
                documents as d \
                on a.document_id == d.iid \
                WHERE a.document_id LIKE ? \
                AND a.type LIKE "highlight" \
                AND (e.text IS null or e.text == "")'
    if document_id:
        cur = execute(needs_sql, (document_id,))
    else:
        cur = execute(needs_sql, ('%',))
    
    return cur.fetchall()

def store_extracts(annotation_id, text):
    ''' Insert text extracted from a pdf for a Mendeley annotation'''
    data = (annotation_id, text)
    extract_sql = 'INSERT OR REPLACE INTO extracts VALUES (?,?)'
    execute(extract_sql, data)


def count_rows(table_name):
    cur = execute('SELECT COUNT(*) FROM {}'.format(table_name))
    return cur.fetchone()[0]

def get_summary(document_id=None):
    needs_sql = 'SELECT d.iid, d.title, d.authors, d.abstract, \
                a.id, a.type, a.text, a.positions, a.last_modified, \
                e.text FROM \
                annotations AS a \
                LEFT OUTER JOIN \
                extracts as e \
                ON  e.anno_id == a.id \
                JOIN \
                documents as d \
                on a.document_id == d.iid \
                WHERE a.document_id LIKE ? \
                AND \
                ((e.text IS NOT null AND e.text != "") \
                OR (a.text IS NOT null))'
    if document_id:
        cur = execute(needs_sql, (document_id,))
    else:
        cur = execute(needs_sql, ('%',))

    return cur.fetchall()
