---
date: {{ data.date }}
title: "{{ data.title }}"
tags:
 - "paper"
 - "mendeley"
---

Authors: {{ data.authors }}

## Abstract
{{ data.abstract }}

## Notes

{% for page_num, pages in data.pages %}
### Page {{ page_num }}
{% for note in pages %}
{% if note.style== 'highlight' %}> {{ note.text }}{%endif %}{% if note.style == 
'sticky_note' %}- {{ note.text }}{% endif %}

<!-- -->
{% endfor %}
{% endfor %}
